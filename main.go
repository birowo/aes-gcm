package main

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"fmt"
	"net/http"
	"strconv"
)

type Bs []byte

func (bs Bs) String() string {
	ret := "["
	for _, b := range bs {
		ret += strconv.Itoa(int(b)) + ","
	}
	return ret[:len(ret)-1] + "]"
}

const IVLen = 12

func GenIV(iv []byte) error {
	l := 0
	for l < IVLen {
		n, err := rand.Read(iv[l:IVLen])
		if err != nil {
			return err
		}
		l += n
	}
	return nil
}
func Encrypt(plain, key, iv []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	aead, err := cipher.NewGCM(block)
	if err != nil {
		return nil, err
	}
	return aead.Seal(nil, iv, plain, nil), nil
}

var rootHtm = []byte(`
plain....: <input id="plain"><br>
key......: <input id="pswd"> (16 chrs)<br>
<button onclick="send()">encrypt</button><br>
decrypted: <span id="dec"></span>
<script>
function decrypt(pswd, iv, enc, rslt){
	window.crypto.subtle.importKey(
		'raw',pswd,{name:'AES-GCM'},false,['decrypt']
	).then(function(key){
		window.crypto.subtle.decrypt(
			{name:'AES-GCM',iv:iv},key,enc
		).then(rslt)
	})
}
function post(url, rspnType, posted, rspn){
	var xhr = new XMLHttpRequest();
	xhr.open('POST', url, true);
	xhr.responseType = rspnType
	xhr.onload = function(){rspn(xhr.response)}
	xhr.send(posted);
}
function send(){
	if(pswd.value.length<16 || plain.value==''){return}
	var pswd_ = pswd.value.substring(0,16)
	var pswd__ = (new TextEncoder()).encode(pswd_)
	post('/encrypt', 'arraybuffer', pswd_+plain.value, function(rspn){
		var iv = rspn.slice(0,12)
		var enc = rspn.slice(12)
		decrypt(pswd__, iv, enc, function(plain_){
			dec.textContent = (new TextDecoder()).decode(plain_)
		})
	})
}
</script>
`)

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Header()["Content-Type"] = []string{"text/html; charset=utf-8"}
		w.Write(rootHtm)
	})
	http.HandleFunc("/encrypt", func(w http.ResponseWriter, r *http.Request) {
		bfr := make([]byte, 999)
		if err := GenIV(bfr); err != nil {
			println(err.Error())
			return
		}
		posted := bfr[IVLen:]
		n, _ := r.Body.Read(posted)
		if n < 17 {
			println("empty plain input")
			return
		}
		iv := bfr[:IVLen]
		pswd := posted[:16]
		plain := posted[16:n]
		fmt.Println("pswd:", Bs(pswd), "\nplain:", Bs(plain), "\niv:", Bs(iv))
		enc, err := Encrypt(plain, pswd, iv)
		if err != nil {
			println(err.Error())
			return
		}
		fmt.Println("\nenc:", Bs(enc))
		l := copy(posted, enc)
		w.Write(bfr[:IVLen+l])
	})
	http.ListenAndServe(":8080", nil)
}
